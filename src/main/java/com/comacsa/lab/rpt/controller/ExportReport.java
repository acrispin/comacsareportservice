/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comacsa.lab.rpt.controller;

import com.comacsa.lab.util.AppSqlConfig;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.query.JRXPathQueryExecuterFactory;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ACRISPIN
 */
//@WebServlet(name = "ExportReport", urlPatterns = {"/ExportReport"})
public class ExportReport extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ExportReport.class);
    private static final String PATH_REPORT_PATTERN = "/reports/%s.jasper";
    private static final ThreadLocal<DateFormat> SDF = new ThreadLocal<DateFormat>(){
        @Override
        protected DateFormat initialValue() {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("INIT SimpleDateFormat - " + Thread.currentThread().getName());
            }            
            return new SimpleDateFormat("dd/MM/yyyy");
        }
    };
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.text.ParseException ParseException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug("................processRequest INI....................");
        }
        
        String tipo = request.getParameter("tipo");
        String reporte = request.getParameter("reporte");
        
        Map<String, Object> parameters;        
        
        switch(reporte){
            case "productoFino":
            case "productoGrueso":
                parameters = productoParams(request);
                break;
            default:
                parameters = new HashMap<>();
                parameters.put("fecha", request.getParameter("fecha"));
                parameters.put("user", request.getParameter("user"));
                break;
        }
        
        String pathRpt = String.format(PATH_REPORT_PATTERN, reporte);        
        parameters.put(JRParameter.REPORT_LOCALE, Locale.US);
        
        try (ServletOutputStream servletOutputStream = response.getOutputStream()) {
            File reportFile = new File(getServletConfig().getServletContext().getRealPath(pathRpt));
            Connection cn = null;
            byte[] bytes = null;  
            try {
                cn = AppSqlConfig.getSqlSessionFactory().openSession().getConnection();
                // cn.setAutoCommit(true);
                switch (tipo) {
                    case "pdf":
                        bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameters, cn);
                        response.setContentType("application/pdf");
                        // response.setHeader("Content-disposition", "attachment; filename=" + rptName + ".pdf");
                        break;
                    case "xls":
                        parameters.put(JRParameter.IS_IGNORE_PAGINATION, true);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(reportFile.getPath(), parameters, cn);
                        JRXlsExporter exporterXLS = new JRXlsExporter();
                        ExporterInput exporterInput = new SimpleExporterInput(jasperPrint);
                        exporterXLS.setExporterInput(exporterInput);
                        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
                            SimpleOutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(bos);
                            exporterXLS.setExporterOutput(exporterOutput);
                            exporterXLS.exportReport();
                            bytes = bos.toByteArray();
                        }
                        response.setContentType("application/vnd.ms-excel");
                        response.setHeader("Content-disposition", "attachment; filename=" + reporte + ".xls");
                        break;
                    default:
                        break;
                }
            } catch (JRException ex) {
                LOGGER.error(ex.getMessage());
            } finally {
                if(cn != null){
                    try {
                        cn.close();
                    } catch (SQLException ex) {
                        LOGGER.error(ex.getMessage());
                    }
                }
            }
            
            if (bytes != null) {
                response.setContentLength(bytes.length);
                servletOutputStream.write(bytes, 0, bytes.length);
                servletOutputStream.flush();
            }
        }
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("................processRequest FIN....................");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if(LOGGER.isDebugEnabled()){
                LOGGER.debug("...........GET................");
            }
            processRequest(request, response);
        } catch (ParseException ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if(LOGGER.isDebugEnabled()){
                LOGGER.debug("...........POST................");
            }
            processRequest(request, response);
        } catch (ParseException ex) {
            LOGGER.error(ex.getMessage());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private static Map getJasperParams() {
        Map params = new HashMap();
        params.put(JRXPathQueryExecuterFactory.XML_DATE_PATTERN, "dd/MM/yyyy");
        params.put(JRXPathQueryExecuterFactory.XML_NUMBER_PATTERN, "#,##0.##");
        params.put(JRXPathQueryExecuterFactory.XML_LOCALE, Locale.ENGLISH);
        params.put(JRParameter.REPORT_LOCALE, Locale.US);
        return params;
    }
    
    private Map<String, Object> productoParams(HttpServletRequest req) 
            throws ParseException {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("fechaProduccion", req.getParameter("fecha"));
        parameters.put("user", req.getParameter("user"));
        // parameters.put("fechaProduccion", SDF.get().parse(req.getParameter("fecha")));        
        return parameters;
    }
       
}