/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comacsa.lab.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.ResourceBundle;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ACRISPIN
 */
public final class AppSqlConfig {

    private static final Logger LOGGER = LogManager.getLogger(AppSqlConfig.class);
    private static final String RESOURCE = "sqlMapConfig.xml";
    private static SqlSessionFactory sqlSessionFactory;

    protected AppSqlConfig() {
        throw new UnsupportedOperationException();
    }

    /**
     * Obtiene la referencia al SqlMapClient del Framework IBatis.
     *
     * @return referencia a SqlMapClient
     */
    public static synchronized SqlSessionFactory getSqlSessionFactory() {
        if (sqlSessionFactory == null) {
            sqlSessionFactory = init("produccion");
        }
        return sqlSessionFactory;
    }
    
    private static SqlSessionFactory init(String enviroment) {
        LOGGER.info("----------------- Usando conexión produccion -----------------");
        SqlSessionFactory ssf;
        try (InputStream inputStream = Resources.getResourceAsStream(RESOURCE)) {
            ssf = new SqlSessionFactoryBuilder().build(inputStream, enviroment);
            if (ssf == null || 
                ssf.getConfiguration() == null ||
                ssf.getConfiguration().getEnvironment() == null) {
                throw new RuntimeException(String.format("Environment '%s' incorrecto", enviroment));
            }
        } catch (IOException | RuntimeException ex) {
            LOGGER.error(ex.getMessage(), ex);
            ssf = initDitrect();
        }
        return ssf;
    }
    
    private static SqlSessionFactory initDitrect() {
        SqlSessionFactory ssf = null;
        if (!LOGGER.isDebugEnabled()) {
            LOGGER.info("Solo se usa conexion directa en modo DEBUG.");
            return ssf;
        }
        LOGGER.info("----------------- Usando conexión directa -----------------");
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("application");
            Properties defaultProps = new Properties();
            String direccionUrl = bundle.getString("database.url.format");
            String url = String.format(direccionUrl,
                    bundle.getString("database.server"),
                    bundle.getString("database.port"),
                    bundle.getString("database.dbname"));
            defaultProps.put("driver", bundle.getString("database.driver"));
            defaultProps.put("url", url);
            defaultProps.put("username", bundle.getString("database.username"));
            defaultProps.put("password", bundle.getString("database.password"));
            try (InputStream inputStream = Resources.getResourceAsStream(RESOURCE)) {
                ssf = new SqlSessionFactoryBuilder().build(inputStream, "junit", defaultProps);
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return ssf;
    }

}
