/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comacsa.lab.util;

import java.util.ResourceBundle;

/**
 *
 * @author ACRISPIN
 */
public final class Resources {

    private static Resources instance;
    private ResourceBundle bundle;

    private Resources() {
    }

    /**
     * Retorna el objeto Resources conteniendo los archivos properties 
     * @return the instance
     */
    public static Resources getInstance() {
        if (instance == null) {
            synchronized (Resources.class) {
                if(instance == null){
                    instance = new Resources();
                }
            }
        }
        return instance;
    }
    
    /**
     * Retorna el valor de una propiedad
     * @param key property
     * @return the instance
     */
    public static String getValue(String key) {
       return getInstance().getBundle().getString(key);
    }

    /**
     * Retorna el objeto ResourceBundle conteniendo los archivos properties
     * @return the bundle
     */
    public ResourceBundle getBundle() {
        if (bundle == null) {
            bundle = ResourceBundle.getBundle("application");
        }
        return bundle;
    }

}